<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<center><img src="images/emp2.jpg"></center>
<br><br>
<center>
<form:form modelAttribute="insert" action="insertEmp">
<table border=1 bgcolor="#7FFFD4" cellspacing="5" cellpadding="6">
	<tr><td>Insert New Employee</td></tr>
	<tr><td>Employee Name:</td>
		<td><form:input path="employee_name" required="required" /></td>
	</tr>
	<tr><td>Employee NRC:</td>
		<td><form:input path="employee_nrc" required="required"/></td>
	</tr>
	<tr><td>Phone No:</td>
		<td><form:input path="phone_no" required="required"/></td>
	</tr>
	<tr><td>Address:</td>
		<td><form:input path="address" required="required"/></td>
	</tr>
	<tr><td>Age:</td>
		<td><form:input path="age" required="required"/></td>
	</tr>
	<tr><td>Department:</td>
		<td><form:select path="department.department_id">
			<form:options items="${requestScope.departmentList}" itemLabel="department_name" itemValue="department_id" />
		</form:select></td>
		
	</tr>
	
</table>
<br>
<form:button type="submit" style="font-size:larger;background-color:#7FFFD4" >Register</form:button>
<form:button type="reset" style="font-size:larger;background-color:#7FFFD4">Cancel</form:button>
	
</form:form>
</center>
</body>
</html>