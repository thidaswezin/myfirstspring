<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<spring:url value="http://code.jquery.com/jquery-1.11.3.min.js"
	var="jqueyJS" />
<script src="${jqueyJS}"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<center>
<font size=6 color=blue>Department with Employee List</font>
<br><br><br>
<img src="images/emp4.jpg">
<br><br><br><br>
<form:form modelAttribute="userObject">
		<form:select path="department.department_id" id="departmentId" onchange="callAjax()">
		<form:options items="${requestScope.departmentList}" itemLabel="department_name" itemValue="department_id" />
		</form:select>
		<form:select path="employee_id" id="employeeId"></form:select>
</form:form>
<script type="text/javascript">
		function callAjax() {
			$('#employeeId').empty();
			console.log("call ajax function");
			var departmentId = $("#departmentId").val(); //Get city id   id="cityId"
			console.log("departmentId="+departmentId);
			$.ajax({
				type : 'POST',
				url : "callAjaxTest",
				contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
				data : {
					departmentId : departmentId
				},
				success : function(response) {
					console.log(response);
					var jason = $.parseJSON(response);
					console.log(jason[0].employee_id);
					$('#employeeId').empty();

					for ( var i in jason) {
						$('#employeeId').append('<option value="' + jason[i].employee_id + '">' + jason[i].employee_name + '</option>')

					}

				},
				error : function(xmlHttpRequest, textStatus, errorThrown) {
					console.log("error" + errorThrown);
					if (xmlHttpRequest.readyState = 0 || xmlHttpRequest.status == 0)

						return;
				}
			});
		}
	</script>
</center>
</body>
</html>