<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body bgcolor="#7FFFD4">
<br><br>
<table border=1 cellspacing="5" cellpadding="8">

<tr>
	<th>Employee Id</th>
	<th>${userData.employee_id}</th>

</tr>
<tr>
	<td>Name</td>
	<td>${userData.employee_name}</td>

</tr>
<tr>
	<td>NRC</td>
	<td>${userData.employee_nrc}</td>

</tr>
<tr>
	<td>Phone No</td>
	<td>${userData.phone_no}</td>

</tr>
<tr>
	<td>Address</td>
	<td>${userData.address}</td>

</tr>
<tr>
	<td>Department</td>
	<td>${userData.department.department_name}</td>

</tr>


</table>
</body>
</html>