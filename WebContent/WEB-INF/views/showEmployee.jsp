<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<h1><center><font color="blue">All Employees</font></center></h1>
<br>
<a href="insertEmployee"><img src="images/add1.png"></a><font size=6 color="blue">Employee</font>
<br><br>
<table border="1" bgcolor="#7FFFD4">
	<thead>
	<tr>
		<th>ID</th>
		<th> Name</th>
		<th> NRC</th>
		<th> Phone Number</th>
		<th> Address</th>
		<th> Department</th>
		<th> Action</th>
		
	</tr>
	</thead>
	<tbody>
	<c:forEach items="${employeeList}" var="employeeList">
	<tr>
		<td>${employeeList.employee_id}</td>
		<td>${employeeList.employee_name}</td>
		<td>${employeeList.employee_nrc}</td>
		<td>${employeeList.phone_no}</td>
		<td>${employeeList.address}</td>
		<td>${employeeList.department.department_name}</td>
		<td><a href="viewEmployee?employee_id=${employeeList.employee_id}" class="btn-btn-primary"><img src="images/view.png" /></a>
			<a href="updateEmployee?employee_id=${employeeList.employee_id}" class="btn-btn-primary"><img src="images/edit.png" /></a>
			<a href="deleteEmployee?employee_id=${employeeList.employee_id}" class="btn-btn-primary"><img src="images/delete.png" /></a>
		</td>
		
		<%-- <td><a href="update?user_id=${userList.user_id}" class="btn-btn-primary">Update User</a></td>
		<td><a href="delete?user_id=${userList.user_id}" class="btn-btn-primary">Delete User</a></td> --%>
	</tr>
	</c:forEach>
	</tbody>
	
</table>
</body>
</html>