package com.isgm.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.swing.JOptionPane;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.isgm.entity.Department;
import com.isgm.entity.Employee;
import com.isgm.models.AjaxModel;
import com.isgm.service.EmployeeManagementService;

@Controller
public class EmployeeManagementController {
	@Autowired
	@Qualifier("testService")
	EmployeeManagementService myservice;
	
	@RequestMapping("allemployee")
	public String showEmployee(Model model) {
		List<Employee> employeeList = myservice.getAllEmployeeList();
		model.addAttribute("employeeList",employeeList);
		model.addAttribute("show", new Employee());
		return "showEmployee";
	}
	
	
	
	@RequestMapping("insertEmployee")
	public String welcome(Model model) {
		List<Department> departmentList = myservice.getAllDepartmentList();
		model.addAttribute("insert", new Employee());
		model.addAttribute("departmentList", departmentList);
		return "insertEmployee";

	}
	
	@RequestMapping(value = "insertEmp", method = RequestMethod.POST)
	public String insertEmployee(@ModelAttribute("insert")Employee employee,Model model) {
		
		myservice.insertEmployeeData(employee);
		List<Department> departmentList = myservice.getAllDepartmentList();
		model.addAttribute("departmentList", departmentList);
		model.addAttribute("insert", new Employee());
		return "insertEmployee";

	}
	
	@RequestMapping("updateEmployee")
	public String updateuser(@RequestParam("employee_id")Integer employee_id,Model model) {
		List<Employee> employeeList = myservice.getAllEmployeeList();
		List<Department> departmentList = myservice.getAllDepartmentList();
		
		Employee employeeData=myservice.getUserById(employee_id);
		
		model.addAttribute("employeeList", employeeList);
		model.addAttribute("userData", employeeData);
		model.addAttribute("departmentList", departmentList);
		//model.addAttribute("user", new User());
		return "updateEmployee";
	}
	
	@RequestMapping(value = "updateEmployee1", method = RequestMethod.POST)
	public String UpdateUser(@ModelAttribute("userData")Employee employee, Model model) {

		myservice.updateEmployeeData(employee);
		List<Department> departmentList = myservice.getAllDepartmentList();
		List<Employee> employeeList = myservice.getAllEmployeeList();
		model.addAttribute("employeeList", employeeList);
		model.addAttribute("departmentList", departmentList);
		
		return "showEmployee";

	}
	
	@RequestMapping("viewEmployee")
	public String viewEmployee(@RequestParam("employee_id")Integer employee_id,Model model) {
		
		
		Employee employeeData=myservice.getUserById(employee_id);
		
		
		model.addAttribute("userData", employeeData);
		
		//model.addAttribute("user", new User());
		return "viewEmployeeData";
	}
	
	@RequestMapping(value="ajaxTest")
	public String AjaxTest(Model model) {
		List<Department> departmentList = myservice.getAllDepartmentList();
		model.addAttribute("departmentList", departmentList);
		model.addAttribute("userObject", new Employee());
		return "Ajax";

	}
	
	@RequestMapping(value = "callAjaxTest", method = RequestMethod.POST)
	@ResponseBody
	public String callAjaxTest(@RequestParam("departmentId") Integer departmentId, Model model, HttpServletRequest session) {
		// myTestService.updateCityData(city);
		List<Employee> employee = myservice.getEmployeeByDepartmentId(departmentId);
		//System.out.println("user list in controller>>>>>>>>>>="+user);
		List<AjaxModel> modelList = new ArrayList<AjaxModel>();

		for (int i = 0; i < employee.size(); i++) {
			// ajaxModel=new AjaxModel();
			AjaxModel ajaxModel = new AjaxModel();
			ajaxModel.setEmployee_id(employee.get(i).getEmployee_id());
			ajaxModel.setEmployee_name(employee.get(i).getEmployee_name());
			modelList.add(ajaxModel);
		}
		System.out.println("model list="+modelList);
		String jasonData = null;
		ObjectMapper mapper = new ObjectMapper();
		if (!employee.isEmpty()) {
			try {
				jasonData = mapper.writeValueAsString(modelList);
			} catch (JsonGenerationException e) {

			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println(jasonData + "<<return jason");
		return jasonData;

	}
	
	@RequestMapping("deleteEmployee")
	public String delete(@RequestParam("employee_id")Integer employee_id,Model model) {
		int dialogButton = JOptionPane.YES_NO_OPTION;
		int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure want to delete","Message",dialogButton);
		if(dialogResult == JOptionPane.YES_OPTION){
		  // Saving code here
			myservice.deleteEmployeeById(employee_id);
			List<Employee> employeeList = myservice.getAllEmployeeList();
			model.addAttribute("employeeList", employeeList);
			//model.addAttribute("user",new User());
			return "showEmployee";
		}
		else {
			List<Employee> employeeList = myservice.getAllEmployeeList();
			model.addAttribute("employeeList", employeeList);
			return "showEmployee";
		}
		

	}
	
}
