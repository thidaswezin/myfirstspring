package com.isgm.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.isgm.entity.Department;
import com.isgm.entity.Employee;


@Repository
public class EmployeeManagementImpDao implements EmployeeManagementDao{
	@Autowired
	private SessionFactory mySessionFactory;
	@Override
	public void insertEmployeeData(Employee employee) {
		// TODO Auto-generated method stub
		mySessionFactory.getCurrentSession().save(employee);
	}
	@Override
	public List<Department> getAllDepartmentList() {
		// TODO Auto-generated method stub
		List<Department> departmentList = mySessionFactory.getCurrentSession()
				.createQuery("select d from Department d")
				.getResultList();
		return departmentList;
	}
	@Override
	public List<Employee> getAllEmployeeList() {
		// TODO Auto-generated method stub
		List<Employee> employeeList = mySessionFactory.getCurrentSession()
				.createQuery("select e from Employee e")
				.getResultList();
		return employeeList;
	}
	@Override
	public Employee getUserById(Integer employee_id) {
		// TODO Auto-generated method stub
		Employee employee=(Employee)mySessionFactory.getCurrentSession().createQuery("select e from Employee e where e.employee_id=:id").setParameter("id", employee_id).getSingleResult();
		return employee;
	}
	@Override
	public void updateEmployeeData(Employee employee) {
		// TODO Auto-generated method stub
		mySessionFactory.getCurrentSession().saveOrUpdate(employee);
	}
	@Override
	public List<Employee> getEmployeeByDepartmentId(Integer departmentId) {
		// TODO Auto-generated method stub
		List<Employee> user=mySessionFactory.getCurrentSession().createQuery("Select e from Employee e where e.department.department_id=:id").setParameter("id", departmentId).getResultList();
		//System.out.println("user list in dao="+user);
		return user;
	}
	@Override
	public void deleteEmployeeById(Integer employee_id) {
		// TODO Auto-generated method stub
		Employee user=getUserById(employee_id);
		mySessionFactory.getCurrentSession().delete(user);
	}

}
