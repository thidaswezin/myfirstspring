package com.isgm.dao;

import java.util.List;

import com.isgm.entity.Department;
import com.isgm.entity.Employee;

public interface EmployeeManagementDao {

	void insertEmployeeData(Employee employee);

	List<Department> getAllDepartmentList();

	List<Employee> getAllEmployeeList();

	Employee getUserById(Integer employee_id);

	void updateEmployeeData(Employee employee);

	List<Employee> getEmployeeByDepartmentId(Integer departmentId);

	void deleteEmployeeById(Integer employee_id);

}
