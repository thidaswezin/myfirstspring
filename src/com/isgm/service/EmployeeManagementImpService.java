package com.isgm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.isgm.dao.EmployeeManagementDao;
import com.isgm.entity.Department;
import com.isgm.entity.Employee;

@Service("testService")
@Transactional
public class EmployeeManagementImpService implements EmployeeManagementService {
	@Autowired
	EmployeeManagementDao mydao;
	@Override
	public void insertEmployeeData(Employee employee) {
		// TODO Auto-generated method stub
		mydao.insertEmployeeData(employee);
	}
	@Override
	public List<Department> getAllDepartmentList() {
		// TODO Auto-generated method stub
		return mydao.getAllDepartmentList();
	}
	@Override
	public List<Employee> getAllEmployeeList() {
		// TODO Auto-generated method stub
		return mydao.getAllEmployeeList();
	}
	@Override
	public Employee getUserById(Integer employee_id) {
		// TODO Auto-generated method stub
		return mydao.getUserById(employee_id);
	}
	@Override
	public void updateEmployeeData(Employee employee) {
		// TODO Auto-generated method stub
		mydao.updateEmployeeData(employee);
	}
	@Override
	public List<Employee> getEmployeeByDepartmentId(Integer departmentId) {
		// TODO Auto-generated method stub
		return mydao.getEmployeeByDepartmentId(departmentId);
	}
	@Override
	public void deleteEmployeeById(Integer employee_id) {
		// TODO Auto-generated method stub
		mydao.deleteEmployeeById(employee_id);
	}

}
